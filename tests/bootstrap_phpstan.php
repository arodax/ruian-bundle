<?php

/*
 * This file is part of the ARODAX Admin package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

/**
 * Initial function to run before the PHPStan analyse.
 *
 * @author Daniel Chodusov <daniel@chodusov.com>
 *
 * @throws Exception
 */
function bootstrap(): void
{
    $autoloadFile = dirname(__DIR__).'/vendor/autoload.php';

    if (!file_exists($autoloadFile)) {
        $autoloadFile = dirname(__DIR__) . '/../../vendor/autoload.php';
        if (!file_exists($autoloadFile)) {
            throw new RuntimeException('Install dependencies to run the static analyse.');
        }
    }

    require_once $autoloadFile;

    $kernel = new \Arodax\AdminBundle\Tests\Kernel();
    $kernel->boot();

    $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
    $application->setAutoExit(false);

    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
        'command' => 'cache:warmup',
    ]));

    $kernel->shutdown();
}

bootstrap();
