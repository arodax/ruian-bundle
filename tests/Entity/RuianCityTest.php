<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use Arodax\RuianBundle\Entity\RuianCity;

class RuianCityTest extends AbstractEntityTestCase
{
    protected RuianCity $object;

    public function setUp(): void
    {
        $this->object = new RuianCity();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'cityCode',
            'cityName',
            'district',
            'competent',
            'coords',
            'data'
        ]);
    }
}
