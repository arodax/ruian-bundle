<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use Arodax\RuianBundle\Entity\CadastralOffice;

class CadastralOfficeTest extends AbstractEntityTestCase
{
    protected CadastralOffice $object;

    public function setUp(): void
    {
        $this->object = new CadastralOffice();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'code',
            'cityName',
            'regionName',
            'countyName',
            'officeName',
        ]);
    }
}
