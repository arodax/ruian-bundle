<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use Arodax\RuianBundle\Entity\RuianDistrict;

class RuianDistrictTest extends AbstractEntityTestCase
{
    protected RuianDistrict $object;

    public function setUp(): void
    {
        $this->object = new RuianDistrict();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'districtCode',
            'districtName',
            'county',
            'city',
            'coords',
            'slug'
        ]);
    }
}
