<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;

abstract class AbstractEntityTestCase extends TestCase
{
    /**
     * Test expected properties of the given entity.
     *
     * @param object $entity             the entity which will be tested
     * @param array  $expectedProperties expected properties
     *
     * @throws \ReflectionException if reflection of the entity could not be achieved
     */
    protected function assertProperties(object $entity, array $expectedProperties = []): void
    {
        $reflectedEntity = new \ReflectionClass($entity);

        // the entity must not contain property which is not listed among expected properties
        foreach ($reflectedEntity->getProperties() as $property) {
            $this->assertContains($property->getName(), $expectedProperties, sprintf('Entity \'%s\' must not contain property \'%s\' because it\'s not listed in expected properties.', $reflectedEntity->getName(), $property->getName()));
        }

        // the entity must comtain property which is lited in expected properties
        foreach ($expectedProperties as $expectedProperty) {
            $this->assertClassHasAttribute($expectedProperty, \get_class($entity), sprintf('Entity \'%s\' must contain property \'%s\' because it\'s listed in expected properties.', $reflectedEntity->getName(), $expectedProperty));
        }
    }
}
