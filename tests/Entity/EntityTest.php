<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use Symfony\Component\Finder\Finder;

class EntityTest extends AbstractEntityTestCase
{
    public function testEnity()
    {
        $finder = new Finder();
        $files = $finder->in(dirname(__DIR__) . '/../src/Entity')->name('*.php')->notName('*Trait.php');
        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            $relativeEntityPath = str_replace(dirname(__DIR__) . '/../src/Entity', '', $file->getPathname());
            $classFileName = $file->getBasename('.'.$file->getExtension());
            $testFileName = '/Entity/'.ltrim(rtrim(str_replace($file->getBasename(), '', $relativeEntityPath), '/').'/'.$classFileName.'Test.php', '/');
            $exist = dirname(__DIR__).$testFileName;
            $this->assertTrue(file_exists($exist), sprintf('Expected to find test in \'%s\' but this file does not exist.', $exist));
        }
    }
}
