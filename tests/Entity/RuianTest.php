<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Tests\Entity;

use Arodax\RuianBundle\Entity\Ruian;

class RuianTest extends AbstractEntityTestCase
{
    protected Ruian $object;

    public function setUp(): void
    {
        $this->object = new Ruian();
    }

    public function testProperties()
    {
        $this->assertProperties($this->object, [
            'adm',
            'cityCode',
            'cityName',
            'momcCode',
            'momcName',
            'mopCode',
            'mopName',
            'districtCode',
            'districtName',
            'streetCode',
            'streetName',
            'so',
            'houseNumber',
            'streetNumber',
            'streetNumberCode',
            'postcode',
            'countyCode',
            'countyName',
            'x',
            'y',
            'dateOfCreation',
            'coords',
            'enabled',
        ]);
    }
}
