<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Search;

use Arodax\RuianBundle\Entity\Ruian;
use Arodax\RuianBundle\Entity\RuianCity;
use Arodax\RuianBundle\Entity\RuianCounty;
use Arodax\RuianBundle\Entity\RuianDistrict;
use Doctrine\ORM\EntityManagerInterface;

class RuianSearchDoctrineProvider implements
    RuianAdmSearchProviderInterface,
    RuianCitySearchProviderInterface,
    RuianDistrictSearchProviderInterface,
    RuianCountySearchProviderInterface
{
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function searchAdm(string $search, int $limit = 20): array
    {
        /**
         * @var \Doctrine\ORM\QueryBuilder $qb
         */
        $qb = $this->em->getRepository(Ruian::class)->createQueryBuilder('r');

        $or = $qb->expr()->orX();
        $or->add($qb->expr()->like('r.cityName', ':search'));
        $or->add($qb->expr()->like('r.districtName', ':search'));
        $or->add($qb->expr()->like('r.streetName', ':search'));
        $or->add($qb->expr()->like('r.adm', ':search'));

        $qb
            ->select('r.adm AS adm, r.streetName AS street_name, r.houseNumber AS house_number, r.districtName AS district_name, r.cityName AS city_name, r.countyName AS county_name, r.postcode AS postcode')
            ->where($or)
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    public function searchCity(string $search, int $limit = 20): array
    {
        /**
         * @var \Doctrine\ORM\QueryBuilder $qb
         */
        $qb = $this->em->getRepository(RuianCity::class)->createQueryBuilder('c');

        $or = $qb->expr()->orX();
        $or->add($qb->expr()->like('c.cityName', ':search'));
        $or->add($qb->expr()->like('c.cityCode', ':search'));

        $qb
            ->select('c.cityName AS city_name, c.cityCode AS city_code')
            ->where($or)
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    public function searchDistrict(string $search, int $limit = 20): array
    {
        /**
         * @var \Doctrine\ORM\QueryBuilder $qb
         */
        $qb = $this->em->getRepository(RuianDistrict::class)->createQueryBuilder('d');

        $or = $qb->expr()->orX();
        $or->add($qb->expr()->like('d.districtName', ':search'));
        $or->add($qb->expr()->like('d.districtCode', ':search'));

        $qb
            ->select('d.districtName AS district_name, d.districtCode AS district_code')
            ->where($or)
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    public function searchCounty(string $search, int $limit = 20): array
    {
        /**
         * @var \Doctrine\ORM\QueryBuilder $qb
         */
        $qb = $this->em->getRepository(RuianCounty::class)->createQueryBuilder('c');

        $or = $qb->expr()->orX();
        $or->add($qb->expr()->like('c.countyName', ':search'));
        $or->add($qb->expr()->like('c.countyCode', ':search'));

        $qb
            ->select('c.countyName AS county_name, c.countyCode AS county_code')
            ->where($or)
            ->setParameter('search', '%'.$search.'%')
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }
}