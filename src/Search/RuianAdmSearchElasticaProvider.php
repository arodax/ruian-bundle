<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Search;

use FOS\ElasticaBundle\Finder\FinderInterface;

class RuianAdmSearchElasticaProvider implements RuianAdmSearchProviderInterface
{
    protected FinderInterface $finder;

    public function __construct(FinderInterface $finder)
    {
        $this->finder = $finder;
    }

    protected function prepareQuery(string $query): string
    {
        $query = strtr($query, [ "/" => "\\/" ]);
        $query = '*'.$query.'*';

        return $query;
    }

    public function searchAdm(string $search, int $limit = 20): array
    {
        return $this->finder->find($this->prepareQuery($search), $limit);
    }
}