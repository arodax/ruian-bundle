<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\EventListener;

use Arodax\RuianBundle\Repository\RuianRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\ElasticaBundle\Persister\ObjectPersisterInterface;
use Arodax\RuianBundle\Event;

class ElasticaListener implements EventSubscriberInterface
{
    private ObjectPersisterInterface $persister;
    private RuianRepository $repository;

    public function __construct(ObjectPersisterInterface $persister, RuianRepository $repository)
    {
        $this->persister = $persister;
        $this->repository = $repository;
    }

    public function indexNewRecords(Event\NewRecordsEvent $event)
    {
        foreach ($event->getRecords() as $record) {
            $ruian = $this->repository->findOneBy($record);
            $this->persister->replaceOne($ruian);
        }
    }

    public function indexUpdatedRecords(Event\UpdatedRecordsEvent $event)
    {
        foreach ($event->getRecords() as $record) {
            $ruian = $this->repository->findOneBy($record);
            $this->persister->replaceOne($ruian);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            Event\NewRecordsEvent::class => 'indexNewRecords',
            Event\UpdatedRecordsEvent::class => 'indexUpdatedRecords',
        ];
    }
}
