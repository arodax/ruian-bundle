<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\HttpClient;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Symfony\Component\String\u;

class ElasticClient
{
    private const NUMBER_OF_RESULTS = 10;
    private HttpClientInterface $httpClient;
    private ?string $elasticUrl;

    public function __construct(?string $elasticUrl)
    {
        $this->httpClient = HttpClient::create();
        $this->elasticUrl = $elasticUrl;
    }

    public function search(?string $query): array
    {
        try {
            if (null === $this->elasticUrl) {
                return [];
            }

            $url = u($this->elasticUrl)->ensureEnd('/_search');

            $response = $this->httpClient->request('GET', $url->toString(), [
                'json' => [
                    'size' => self::NUMBER_OF_RESULTS,
                    'query' => [
                        'bool' => [
                            'must' => [
                                'multi_match' => [
                                    'query' => $query,
                                    'type' => 'most_fields',
                                    'fields' => ['postcode', 'district_name', 'city_name', 'mop_name', 'momc_name', 'street_name', 'house_number', 'street_number']
                                ]
                            ],
                        ]
                    ]
                ]
            ]);

            return $response->toArray();
        } catch (\Throwable $exception) {
            return [];
        }
    }
}
