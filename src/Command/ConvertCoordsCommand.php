<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Repository\RuianRepository;
use Arodax\RuianBundle\Utils\RuianGeocoder;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConvertCoordsCommand extends AbstractCommand
{
    public const COMMAND_NAME = 'arodax_ruian:convert-coords';
    public const COMMAND_DESC = 'Mass convert JTSK to WGS84 for all ruain data';

    protected RuianRepository $repository;
    protected RuianGeocoder $geocoder;

    public function __construct(RuianRepository $repository, RuianGeocoder $geocoder)
    {
        $this->repository = $repository;
        $this->geocoder = $geocoder;

        parent::__construct();
    }

    /**
     * Configure default values for the command.
     */
    protected function configure(): void
    {
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle)
    {
        $this->geocoder->queue();

        return 0;
    }
}
