<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Entity\Ruian;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use FOS\ElasticaBundle\Command\PopulateCommand as ElasticaCommand;

class RuianImportCommand extends AbstractCommand
{
    /**
     * Command name.
     */
    public const COMMAND_NAME = 'arodax_ruian:import:adm';
    public const COMMAND_DESC = 'Import ruian data from files.';

    protected const CSV_HEADER = 'adm,city_code,city_name,momc_code,momc_name,mop_code,mop_name,district_code,district_name,street_code,street_name,so,house_number,street_number,street_number_code,postcode,y,x,date_of_creation';

    protected EntityManagerInterface $manager;
    protected Filesystem $filesystem;
    protected ?string $ruianFile;
    protected ?string $nutsFile;
    protected string $table;

    public function __construct(
        EntityManagerInterface $manager,
        Filesystem $filesystem,
        ?string $ruianFile = null,
        ?string $nutsFile = null
    ) {
        $this->manager = $manager;
        $this->filesystem = $filesystem;
        $this->ruianFile = $ruianFile;
        $this->nutsFile = $nutsFile;

        parent::__construct();

        $meta = $this->manager->getClassMetadata(Ruian::class);
        $this->table = $meta->getTableName();
    }

    protected function configure(): void
    {
        parent::configure();

        $this
            ->addOption('ruian', 'r', InputOption::VALUE_REQUIRED, 'Ruian csv file path. Defaults to <comment>env(APP_RUIAN_FILE)</comment>.', null)
            ->addOption('nuts', 'u', InputOption::VALUE_REQUIRED, 'Nuts sql file path. Defaults to <comment>env(APP_NUTS_FILE)</comment>.', null);

        if (class_exists(ElasticaCommand::class)) {
            $this->addOption('populate-elastic', 'p', InputOption::VALUE_REQUIRED, 'Also populate elastic index.', true);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle): int
    {
        $ruianFile = $input->getOption('ruian') ?: $this->ruianFile;
        $nutsFile = $input->getOption('nuts') ?: $this->nutsFile;

        if (!$this->filesystem->exists($ruianFile)) {
            throw new \InvalidArgumentException(sprintf("file does not exist: %s", $ruianFile));
        }

        if (!$this->filesystem->exists($nutsFile)) {
            throw new \InvalidArgumentException(sprintf("file does not exist: %s", $nutsFile));
        }

        $this->writelnts(sprintf('Loading ruian data from files <comment>%s</comment> and <comment>%s</comment>', $ruianFile, $nutsFile), $symfonyStyle);

        $db = $this->manager->getConnection();
        $db->setAutoCommit(false);

        $db->executeQuery(<<<SQL
        SET FOREIGN_KEY_CHECKS = 0;
        TRUNCATE TABLE {$this->table};
SQL);

        $header = self::CSV_HEADER;
        $db->executeQuery(<<<SQL
        LOAD DATA LOCAL INFILE :file 
        INTO TABLE {$this->table} FIELDS TERMINATED BY ',' IGNORE 1 LINES ({$header});
SQL, [
        ':file' => $ruianFile,
        ]);

        $this->writelnts(sprintf('Ruian file loaded'), $symfonyStyle);

        $this->runAnotherCommand($output, 'doctrine:database:import', ['file' => $nutsFile]);

        $this->writelnts(sprintf('Nuts file loaded'), $symfonyStyle);

        $db->executeQuery(<<<SQL
        UPDATE {$this->table} r LEFT JOIN ruian_nuts rn ON rn.city_code = r.city_code 
        SET r.county_name = rn.county_name, r.county_code = rn.county_code ;
SQL);

        $db->commit();
        $this->writelnts(sprintf('Ruian data imported'), $symfonyStyle);

        if (class_exists(ElasticaCommand::class)) {
            if ($input->getOption('populate-elastic')) {
                $this->runAnotherCommand($output, 'fos:elastica:populate', ['--index' => 'ruian']);
            }
        }

        return 0;
    }
}
