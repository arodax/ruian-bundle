<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Entity\RuianCity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This class contains app:auction:join command.
 */
class RuianCoordsCommand extends AbstractCommand
{
    /**
     * Command name.
     */
    public const COMMAND_NAME = 'arodax_ruian:coords';
    public const COMMAND_DESC = 'Fetch ruian coords.';
    protected const FILE = __DIR__ . '/../../data/dev/souradnice.csv';

    protected EntityManagerInterface $em;
    protected SerializerInterface $serializer;
    protected array $coords = [];

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->serializer = $serializer;
    }

    protected function configure(): void
    {
        parent::configure();

        $this->addOption('replace-existing', 'R', InputOption::VALUE_OPTIONAL, 'Replace existing', false);
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $ui)
    {
        $this->writelnTs('Updating coords', $ui);

        $data = $this->serializer->decode(file_get_contents(self::FILE), 'csv');
        foreach ($data as $key => $val) {
            $this->coords[$val['Kód obce']] = $val;
        }

        /**
         * @var $repository EntityRepository
         */
        $repository = $this->em->getRepository(RuianCity::class);
        $qb = $repository->createQueryBuilder('r');

        if (!$input->getOption('replace-existing')) {
            $qb->andWhere($qb->expr()->isNull('r.coords'));
        }

        $i = 0;
        foreach ($qb->getQuery()->getResult() as $nuts) {
            $lat = (float)$this->coords[$nuts->getCode()]['Latitude'];
            $lon = (float)$this->coords[$nuts->getCode()]['Longitude'];

            $nuts->setCoords($lat, $lon);
            $this->em->persist($nuts);

            if ($i++ > 500) {
                $this->em->flush();
                $i = 0;
            }
        }

        $this->em->flush();
        $this->writelnTs('Ruian coords updated', $ui);

        return 0;
    }
}
