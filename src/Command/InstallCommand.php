<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Entity\RuianCounty;
use Arodax\RuianBundle\Entity\RuianDistrict;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\String\Slugger\SluggerInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * This class provides install and upgrade function after arodax/ruian-bundle has been instaleled.
 * Usually, this command should be triggerd by post-install-cmd hook.
 */
final class InstallCommand extends AbstractCommand
{
    public const COMMAND_NAME = 'arodax_ruian:install';
    public const COMMAND_DESC = 'Install or upgrade the bundle.';

    private EntityManagerInterface $entityManager;
    private LoggerInterface $logger;
    private SluggerInterface $slugger;

    public function __construct(
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        SluggerInterface $slugger
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->slugger = $slugger;

        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle): int
    {
        $this->entityManager->getConfiguration()->setSQLLogger(null);

        $this->installCounties();;
        $this->installDistricts();

        return Command::SUCCESS;
    }

    private function installCounties(): void
    {
        $this->updateSchema(RuianCounty::class);
        //@todo: install / update counties
        $this->updateSlugs(RuianCounty::class);
    }

    private function installDistricts(): void
    {
        $this->updateSchema(RuianDistrict::class);
        //@todo: install / update districts
        $this->updateSlugs(RuianDistrict::class);
    }

    /**
     * Update database schema for the single class
     *
     * @param string $className
     */
    private function updateSchema(string $className): void
    {
        $metadata = $this->entityManager->getMetadataFactory()->getMetadataFor($className);

        $schemaTool = new SchemaTool($this->entityManager);
        $this->logger->info('Updating database schema for "{entity}" with query: "{query}"', ['entity' => $className]);
        $schemaTool->updateSchema([$metadata], true);
    }

    /**
     * Update slug for all entities of the given class name, if slug has not been already set and entity
     * has support of settings slug by reading getName and setSlug methods.
     *
     * @param object $entity
     */
    private function updateSlugs(string $className): void
    {
        if (false === property_exists($className, 'slug')) {
            throw new \LogicException(sprintf('Attempted to update slug for entity "%s" but this entity does not have a "slug" property. Did you upgrade database schema after 3.1.0 version?', $className));
        }

        if (false === method_exists($className, 'setSlug')) {
            throw new \LogicException(sprintf('Attempted to update slug for entity "%s" but this entity does not have a "setSlug" method. Did you upgrade database schema after 3.1.0 version?', $className));
        }

        if (false === method_exists($className, 'getName')) {
            throw new \LogicException(sprintf('Attempted to update slug for entity "%s" but this entity does not have a "getName" method. Did you upgrade database schema after 3.1.0 version?', $className));
        }

        $qb = $this->entityManager->createQueryBuilder();

        $entities = $qb->select('e')->from($className, 'e')->where($qb->expr()->eq('e.slug', ':slug'))->setParameter('slug', '')->getQuery()->getResult();

        foreach ($entities as $entity) {
            $slug = $this->slugger->slug($entity->getName(), '-', 'cs-CZ')->lower()->toString();
            $entity->setSlug($slug);
            $this->logger->info('Setting slug {slug} for {name}', ['slug' => $slug, 'name' => $entity->getName()]);
        }

        $this->entityManager->flush();
    }
}
