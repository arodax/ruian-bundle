<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Utils\CuzkPrompter;
use Arodax\RuianBundle\Utils\MapyczPrompter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GeocodeCommand extends AbstractCommand
{
    public const COMMAND_NAME = 'arodax_ruian:geocode';
    public const COMMAND_DESC = 'Geocode given address to RUIAN data format and dump the output to console.';

    protected CuzkPrompter $cuzkPrompter;
    protected MapyczPrompter $mapyczPrompter;

    /**
     * @param CuzkPrompter $cuzkPrompter
     * @param MapyczPrompter $mapyczPrompter
     */
    public function __construct(CuzkPrompter $cuzkPrompter, MapyczPrompter $mapyczPrompter)
    {
        parent::__construct();

        $this->cuzkPrompter = $cuzkPrompter;
        $this->mapyczPrompter = $mapyczPrompter;
    }

    /**
     * Configure default values for the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->addArgument('address', InputArgument::REQUIRED, 'Adddress which should be geocoded by RUIAN service');
        $this->addOption('prompter', 'P', InputOption::VALUE_OPTIONAL, 'Which prompter to use [cuzk, mapycz]. If omitted, use both in this order.');
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle)
    {
        $address = $input->getArgument('address');
        $prompter = $input->getOption('prompter');

        if ($prompter !== 'cuzk') {
            dump('mapycz');
            dump($this->mapyczPrompter->geocode($address));
        }
        if ($prompter !== 'mapycz') {
            dump('cuzk');
            dump($this->cuzkPrompter->geocode($address));
        }

        return 0;
    }
}
