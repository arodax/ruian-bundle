<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Symfony\Component\Console\Input\InputInterface;

final class RuianUpdateDbalCommand extends AbstractRuianUpdate
{
    protected function configure()
    {
        parent::configure();
        $this->setName('arodax_ruian:update_dbal');
    }

    protected function processRow(array $row, int $i, InputInterface $input)
    {
        if (empty($row)) {
            return;
        }

        $tableNames = $this->tmpTable->getTableNames();

        // @fixme: if symfony/messenger is installed and enabled as transport, use second table!
        $table = count($tableNames) > 1 ? $tableNames[1] : $tableNames[0];
        $meta = $this->getMetadata();

        $this->em->getConnection()->insert($table, [
            $meta->getColumnName('adm')           =>  (int)$row[0],
            $meta->getColumnName('cityCode')      =>  !empty($row[1]) ? (int)$row[1] : null,
            $meta->getColumnName('cityName')      =>  !empty($row[2]) ? (string)$row[2] : null,
            $meta->getColumnName('momcCode')      =>  !empty($row[3]) ? (int)$row[3] : null,
            $meta->getColumnName('momcName')      =>  !empty($row[4]) ? (string)$row[4] : null,
            $meta->getColumnName('mopCode')       =>  !empty($row[5]) ? (int)$row[5] : null,
            $meta->getColumnName('mopName')       =>  !empty($row[6]) ? (string)$row[6] : null,
            $meta->getColumnName('districtCode')  =>  !empty($row[7]) ? (int)$row[7] : null,
            $meta->getColumnName('districtName')  =>  !empty($row[8]) ? (string)$row[8] : null,
            $meta->getColumnName('streetCode')    =>  !empty($row[9]) ? (int)$row[9] : null,
            $meta->getColumnName('streetName')    =>  !empty($row[10]) ? (string)$row[10] : null,
            $meta->getColumnName('so')            =>  !empty($row[11]) ? (string)$row[11] : null,
            $meta->getColumnName('houseNumber')   =>  !empty($row[12]) ? (string)$row[12] : null,
            $meta->getColumnName('streetNumber')  =>  !empty($row[13]) ? (string)$row[13] : null,
            $meta->getColumnName('streetNumberCode') =>  !empty($row[14]) ? (int)$row[14] : null,
            $meta->getColumnName('postcode')      =>  !empty($row[15]) ? $row[15] : null,
            $meta->getColumnName('countyCode')    =>  (int)$row[16],
            $meta->getColumnName('countyName')    =>  (string)$row[17],
            $meta->getColumnName('x')             =>  !empty($row[18]) ? (float)$row[18] : null,
            $meta->getColumnName('y')             =>  !empty($row[19]) ? (float)$row[19] : null,
            $meta->getColumnName('dateOfCreation') =>  !empty($row[20]) ? (string)$row[20] : null,
            $meta->getColumnName('enabled')       =>  1
        ]);
    }
}
