<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Entity\RuianDistrict;
use Arodax\RuianBundle\Repository\RuianDataProvider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This class contains app:auction:join command.
 */
class RuianAreasCommand extends AbstractCommand
{
    /**
     * Command name.
     */
    public const COMMAND_NAME = 'arodax_ruian:areas';
    public const COMMAND_DESC = 'Fetch ruian areas.';
    protected const PATH = __DIR__ . '/../../data/dev/area/NUTS3';

    protected EntityManagerInterface $em;
    protected SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        parent::__construct();
        $this->em = $entityManager;
        $this->serializer = $serializer;
    }

    protected function configure(): void
    {
        parent::configure();

        $this->addOption('replace-existing', 'R', InputOption::VALUE_OPTIONAL, 'Replace existing', false);
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $ui)
    {
        $this->writelnTs('Updating areas', $ui);

        $repo = $this->em->getRepository(RuianDistrict::class);
        foreach (RuianDataProvider::countyDistrictMap() as $countyPath => $county) {
            $countyCode = $county[0];
            foreach ($county[1] as $districtPath => $districtCode) {
                $filename = self::PATH.'/'.$countyPath.'/'.$districtPath.'.json';
                $district = $this->loadJsonFile($filename);

                $title = $district['properties']['title'];
                $coords = $district['geometry']['coordinates'][0];

                $district = $repo->find($districtCode);
                if (empty($district)) {
                    dump($title);
                    return 1;
                }

                $district->setCoords($coords);
                $this->em->persist($district);
            }
        }

        $this->em->flush();

        $this->writelnTs('Ruian areas updated', $ui);

        return 0;
    }

    /**
     * Load data from JSON file.
     *
     * @param string $file
     *
     * @throws \Exception
     *
     * @return array
     */
    protected function loadJsonFile(string $file): array
    {
        if (!file_exists($file)) {
            throw new FileNotFoundException(sprintf('Data fixtures file "%s" does not exist. Check if path is correct and file exists', $file));
        }

        $sources = json_decode(file_get_contents($file), true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \Exception(sprintf('Data fixtures file contains invalid JSON - error "%d"', json_last_error()), json_last_error());
        }

        return $sources;
    }
}
