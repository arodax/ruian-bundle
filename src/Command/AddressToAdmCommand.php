<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Utils\CuzkPrompter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddressToAdmCommand extends AbstractCommand
{
    public const COMMAND_NAME = 'arodax_ruian:address-to-adm';
    public const COMMAND_DESC = 'Convert address to ADM and dump the output to console.';

    protected CuzkPrompter $ruianUtility;

    /**
     * @param CuzkPrompter $ruianUtility utility for handling RUIAN
     */
    public function __construct(CuzkPrompter $ruianUtility)
    {
        parent::__construct();

        $this->ruianUtility = $ruianUtility;
    }

    /**
     * Configure default values for the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->addArgument('address', InputArgument::REQUIRED, 'Address which should be parsed by RUIAN service');
    }

    /**
     * {@inheritdoc}
     */
    protected function doExecute(InputInterface $input, ConsoleOutputInterface $output, SymfonyStyle $symfonyStyle)
    {
        dump($this->ruianUtility->addressToAdm($input->getArgument('address')));

        return 0;
    }
}
