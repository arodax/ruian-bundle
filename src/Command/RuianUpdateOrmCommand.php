<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Command;

use Arodax\RuianBundle\Entity\Ruian;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

final class RuianUpdateOrmCommand extends AbstractRuianUpdate
{
    protected function configure()
    {
        parent::configure();
        $this->setName('arodax_ruian:update_orm');
        $this->addOption('flush', 'u', InputOption::VALUE_OPTIONAL, 'Flush every ? iterations.', 100);
    }

    protected function processRow(array $row, int $i, InputInterface $input)
    {
        if (empty($row)) {
            return;
        }

        if ($i > 0 && $i % $input->getOption('flush') === 0) {
            $this->em->flush();
        }

        $ruian = new Ruian();
        $ruian
            ->setAdm((int)$row[0])
            ->setCityCode(!empty($row[1]) ? (int)$row[1] : null)
            ->setCityName(!empty($row[2]) ? (string)$row[2] : null)
            ->setMomcCode(!empty($row[3]) ? (int)$row[3] : null)
            ->setMomcName(!empty($row[4]) ? (string)$row[4] : null)
            ->setMopCode(!empty($row[5]) ? (int)$row[5] : null)
            ->setMopName(!empty($row[6]) ? (string)$row[6] : null)
            ->setDistrictCode(!empty($row[7]) ? (int)$row[7] : null)
            ->setDistrictName(!empty($row[8]) ? (string)$row[8] : null)
            ->setStreetCode(!empty($row[9]) ? (int)$row[9] : null)
            ->setStreetName(!empty($row[10]) ? (string)$row[10] : null)
            ->setSo(!empty($row[11]) ? (string)$row[11] : null)
            ->setHouseNumber(!empty($row[12]) ? (string)$row[12] : null)
            ->setStreetNumber(!empty($row[13]) ? (string)$row[13] : null)
            ->setStreetNumberCode(!empty($row[14]) ? (int)$row[14] : null)
            ->setPostcode(!empty($row[15]) ? $row[15] : null)
            ->setCountyCode((int)$row[16])
            ->setCountyName((string)$row[17])
            ->setX(!empty($row[18]) ? (float)$row[18] : null)
            ->setY(!empty($row[19]) ? (float)$row[19] : null)
            ->setDateOfCreation(!empty($row[20]) ? \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s', (string)$row[20], new \DateTimeZone('Europe/Prague')) : null)
            ->setEnabled(true)
        ;

        $this->em->persist($ruian);
    }

    protected function preProcess(InputInterface $input)
    {
        $this->getMetadata()->setPrimaryTable(['name' => $this->tmpTable->getTableNames()[0]]);
    }
}
