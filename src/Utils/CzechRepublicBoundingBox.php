<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Utils;

final class CzechRepublicBoundingBox
{
    public const BOX = [
        'top_left'      =>  [
            'longitude' =>  12.0276931,
            'latitude'  =>  51.1009756,
        ],
        'bottom_right'  =>  [
            'longitude' =>  18.9161208,
            'latitude'  =>  48.4976897,
        ],
        'center'    =>  [
            'longitude' =>  15.3386383,
            'latitude'  =>  49.7437572,
        ]
    ];

    public static function maxLatitude(): float
    {
        return self::BOX['top_left']['latitude'];
    }

    public static function minLatitude(): float
    {
        return self::BOX['bottom_right']['latitude'];
    }

    public static function maxLongitude(): float
    {
        return self::BOX['bottom_right']['longitude'];
    }

    public static function minLongitude(): float
    {
        return self::BOX['top_left']['longitude'];
    }

    public static function centerLatitude(): float
    {
        return self::BOX['center']['latitude'];
    }

    public static function centerLongitude(): float
    {
        return self::BOX['center']['longitude'];
    }
}