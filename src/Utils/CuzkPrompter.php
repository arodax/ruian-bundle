<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Utils;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class provides method for handling RUIAN data.
 */
class CuzkPrompter
{
    private const OUTPUT_SPATIAL_REFERENCE = '4326';
    private const ESRI_MAP_SERVER = 'https://ags.cuzk.cz/arcgis/rest/services/RUIAN/Vyhledavaci_sluzba_nad_daty_RUIAN/MapServer';

    public function __construct(
        private readonly HttpClientInterface $client
    ) {
    }

    /**
     * Geocode address.
     *
     * @param string $address address for geocoding
     * @param int $maxResults how many geocoded candidates should be returned
     *
     * @return array in ArcGis format
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface when data could not be fetched from ESRI server
     * @throws DecodingExceptionInterface when data could not be decoded.
     */
    public function geocode(string $address, int $maxResults = 1): array
    {
        $response = $this->client->request(
            'GET',
            self::ESRI_MAP_SERVER.'/exts/GeocodeSOE/findAddressCandidates',
            [
                'query' => [
                    'SingleLine' => $address,
                    'maxLocations' => $maxResults,
                    'outSR' => self::OUTPUT_SPATIAL_REFERENCE,
                    'f' => 'json',
                ],
            ]
        );

        $results = [];
        $raw = $response->toArray();

        if (!empty($raw['candidates'])) {
            foreach ($raw['candidates'] as $can) {
                if (!empty($can['location'])) {
                    $x = $can['location']['x'];
                    $y = $can['location']['y'];
                    $title = $can['address'] ?? null;

                    $results[] = [
                        'x' => $x,
                        'y' => $y,
                        'longitude' => $x,
                        'latitude' => $y,
                        'title' => $title,
                    ];
                }
            }
        }

        return $results;
    }

    /**
     * Convert ADM to address.
     *
     * @param int $adm ADM code
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return array in ArcGis format
     */
    public function admToAddress(int $adm): array
    {
        $response = $this->client->request(
            'GET',
            self::ESRI_MAP_SERVER.'/1/query',
            [
                'query' => [
                    'where' => sprintf('kod=%s', $adm),
                    'outFields' => 'adresa',
                    'outSr' => self::OUTPUT_SPATIAL_REFERENCE,
                    'f' => 'json',
                ],
            ]
        );

        return $response->toArray();
    }

    /**
     * Convert address to ADM.
     *
     * @param string $address address exactly as found in RUIAN
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return array in ArcGis format
     */
    public function addressToAdm(string $address): array
    {
        $response = $this->client->request(
            'GET',
            self::ESRI_MAP_SERVER.'/1/query',
            [
                'query' => [
                    'where' => sprintf('adresa=\'%s\'', $address),
                    'outFields' => 'kod',
                    'outSr' => self::OUTPUT_SPATIAL_REFERENCE,
                    'f' => 'json',
                ],
            ]
        );

        return $response->toArray();
    }
}
