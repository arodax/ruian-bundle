<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Utils;

use Arodax\RuianBundle\Entity\Ruian;
use Arodax\RuianBundle\Exception\RuianException;
use Arodax\RuianBundle\Repository\RuianRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RuianGeocoder
{
    private CuzkPrompter $prompter;
    private EntityManagerInterface $em;
    private RuianRepository $repository;
    private LoggerInterface $logger;

    public function __construct(
        CuzkPrompter $prompter,
        EntityManagerInterface $em,
        RuianRepository $repository,
        LoggerInterface $ruianLogger
    ) {
        $this->prompter = $prompter;
        $this->em = $em;
        $this->repository = $repository;
        $this->logger = $ruianLogger;
    }

    public function queue(int $limit = 50)
    {
        $qb = $this->repository->createQueryBuilder('r');
        $qb
            ->setMaxResults($limit)
            ->where($qb->expr()->isNull('r.coords'));

        $ruians = $qb->getQuery()->getResult();
        $count = \count($ruians);

        foreach ($ruians as $ruian) {
            $this->doQueue($ruian);
        }

        $this->logger->info('queued ruian geocode', ['count' => $count]);
    }

    protected function doQueue(Ruian $ruian)
    {
        //override this to use messenger queue
        $this->geocode($ruian);
    }

    public function geocode(Ruian $ruian)
    {
        $em = $this->em;

        try {
            $data = $this->prompter->admToAddress($ruian->getAdm());

            if (empty($data['features'][0]['geometry']['x']) || empty($data['features'][0]['geometry']['y'])) {
                throw new \Exception();
            }

            $longitude = $data['features'][0]['geometry']['x'];
            $latitude = $data['features'][0]['geometry']['y'];

            //TODO: $em->merge() will it work for deserialized?

            $ruian = $this->repository->findOneByAdm($ruian->getAdm());
            if (empty($ruian)) {
                $this->logger->warning('ruian not found', ['adm' => $ruian->getAdm()]);
                throw new RuianException();
            }

            $ruian->setCoords([$longitude, $latitude]);

            $em->persist($ruian);
            $em->flush();

            $this->logger->info('ruian geocoded', [
                'adm' => $ruian->getAdm(),
                'lat' => $ruian->getCoords()->getLatitude(),
                'lon' => $ruian->getCoords()->getLongitude(),
            ]);
        } catch (HttpExceptionInterface $e) {
            $this->logger->warning($e->getMessage(), $e->getResponse()->toArray(false));
            throw new RuianException();
        } catch (TransportExceptionInterface $e) {
            throw new RuianException();
        }
    }
}
