<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Utils;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\NativeHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class provides method for handling RUIAN data.
 */
class MapyczPrompter
{
    private const GEOCODE_URL = 'https://api.mapy.cz/geocode';

    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Geocode address.
     *
     * @param string $address      address for geocoding
     * @param int    $maxResults how many geocoded candidates should be returned
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     *
     * @return array in ArcGis format
     */
    public function geocode(string $address, int $maxResults = 1): array
    {
        $response = $this->httpClient->request(
            'GET',
            static::GEOCODE_URL,
            [
                'query' => [
                    'query' => $address
                ],
                'headers' => [
                    'accept' => 'text/xml'
                ]
            ]
        );

        $crawler = new Crawler($response->getContent());
        $results = [];

        $i = 0;
        foreach ($crawler->filter('result > point > item') as  $item) {
            $x = $item->attributes->getNamedItem('x');
            $y = $item->attributes->getNamedItem('y');
            $x = $x ? $x->nodeValue : null;
            $y = $y ? $y->nodeValue : null;
            $title = $item->attributes->getNamedItem('title');
            $title = $title ? $title->nodeValue : null;


            if ($x && $y) {
                if (!($i++ < $maxResults)) {
                    break;
                }

                $results[] = [
                    'x' => $x,
                    'y' => $y,
                    'longitude' => $x,
                    'latitude' => $y,
                    'title' => $title,
                ];
            }
        }

        return $results;
    }
}
