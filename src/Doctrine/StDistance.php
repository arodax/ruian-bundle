<?php

namespace Arodax\RuianBundle\Doctrine;

use CrEOF\Spatial\ORM\Query\AST\Functions\AbstractSpatialDQLFunction;

/**
 * ST_Contains DQL function.
 *
 * @author  Derek J. Lambert <dlambert@dereklambert.com>
 * @author  Alexandre Tranchant <alexandre.tranchant@gmail.com>
 * @license https://dlambert.mit-license.org MIT
 */
class StDistance extends AbstractSpatialDQLFunction
{
    /**
     * Function SQL name getter.
     *
     * @since 2.0 This function replace the protected property functionName.
     */
    protected function getFunctionName(): string
    {
        return 'ST_Distance';
    }

    /**
     * Maximum number of parameter for the spatial function.
     *
     * @return int the inherited methods shall NOT return null, but 0 when function has no parameter
     * @since 2.0 This function replace the protected property maxGeomExpr.
     *
     */
    protected function getMaxParameter(): int
    {
        return 2;
    }

    /**
     * Minimum number of parameter for the spatial function.
     *
     * @return int the inherited methods shall NOT return null, but 0 when function has no parameter
     * @since 2.0 This function replace the protected property minGeomExpr.
     *
     */
    protected function getMinParameter(): int
    {
        return 2;
    }

    /**
     * Get the platforms accepted.
     *
     * @return string[] a non-empty array of accepted platforms
     * @since 2.0 This function replace the protected property platforms.
     *
     */
    protected function getPlatforms(): array
    {
        return ['postgresql', 'mysql'];
    }
}