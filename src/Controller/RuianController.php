<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Controller;

use Arodax\RuianBundle\Exception\RuianException;
use Arodax\RuianBundle\HttpClient\ElasticClient;
use Arodax\RuianBundle\Repository\RuianDataProviderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RuianController extends AbstractController
{
    protected RuianDataProviderInterface $ruianProvider;

    public function __construct(RuianDataProviderInterface $ruianProvider)
    {
        $this->ruianProvider = $ruianProvider;
    }

    /**
     * @Route("/mapping.{_format}",
     *     name="ruian_mapping",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function mapping()
    {
        return $this->json($this->ruianProvider->getCountyDistrictMap());
    }

    /**
     * @Route("/coords/counties.{_format}",
     *     name="ruian_coords_county_all",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function countyAllCoords()
    {
        return $this->json($this->ruianProvider->getCountyCollection());
    }

    /**
     * @Route("/coords/districts.{_format}",
     *     name="ruian_coords_district_all",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function districtAllCoords()
    {
        return $this->json($this->ruianProvider->getDistrictCollection());
    }

    /**
     * @Route("/coords/districts/{district}.{_format}",
     *     name="ruian_coords_district",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function districtCoords(string $district)
    {
        try {
            if (in_array($district, array_keys($this->ruianProvider->getDistrictList()))) {
                $coords = $this->ruianProvider->getDistrictFeature($this->ruianProvider->getDistrictList()[$district]);
            } else {
                $coords = $this->ruianProvider->getDistrictFeature((int)$district);
            }
        } catch (RuianException $e) {
            throw new NotFoundHttpException('district not found');
        }

        return $this->json($coords);
    }

    /**
     * @Route("/coords/cities.{_format}",
     *     name="ruian_coords_city_all",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function cityAllCoords()
    {
        return $this->json($this->ruianProvider->getCityCollection());
    }

    /**
     * @Route("/coords/cities/{city}.{_format}",
     *     name="ruian_coords_city",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function cityCoords(int $city)
    {
        try {
            $coords = $this->ruianProvider->getCityFeature($city);
        } catch (RuianException $e) {
            throw new NotFoundHttpException('city not found');
        }

        return $this->json($coords);
    }

    /**
     * @Route("/data/cities/{city}.{_format}",
     *     name="ruian_data_city",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     */
    public function cityData(int $city)
    {
        try {
            $data = $this->ruianProvider->getCityData($city);
        } catch (RuianException $e) {
            throw new NotFoundHttpException('city not found');
        }

        return $this->json($data);
    }

    /**
     * @Route("/data/search.{_format}",
     *     name="ruian_data_search",
     *     defaults={"_format": "json"},
     *     requirements={"_format":"json"})
     *
     * @param Request $request
     */
    public function search(Request $request, ElasticClient $client): Response
    {
        if (!$request->query->get('q')) {
            throw new NotFoundHttpException();
        }

        return $this->json($client->search($request->query->get('q')));
    }
}
