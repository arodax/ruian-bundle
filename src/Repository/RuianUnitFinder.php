<?php

declare(strict_types=1);

namespace Arodax\RuianBundle\Repository;

use Arodax\Doctrine\Spatial\ValueObject\Point;
use Arodax\Doctrine\Spatial\ValueObject\Polygon;
use Arodax\RuianBundle\Entity\CompetentCity;
use Arodax\RuianBundle\Entity\Ruian;
use Arodax\RuianBundle\Entity\RuianCounty;
use Arodax\RuianBundle\Entity\RuianDistrict;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\NoResultException;

class RuianUnitFinder
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Point $point
     * @return RuianDistrict
     * @throws NoResultException
     */
    public function findDistrictForPoint(Point $point, bool $exact = true): RuianDistrict
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->em->getRepository(RuianDistrict::class)->createQueryBuilder('d');
        $qb
            ->where('ST_Within(ST_GeomFromText(:geometry), d.coords) = 1')
            ->setParameter('geometry', $point->toWKT(), 'string')
            ->setMaxResults(1);

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            if ($exact) {
                throw $e;
            } else {
                /**
                 * @var QueryBuilder $qb
                 */
                $qb = $this->em->getRepository(RuianDistrict::class)->createQueryBuilder('d');
                $qb
                    ->orderBy('ST_Distance(ST_GeomFromText(:geometry), d.coords)', 'asc')
                    ->setParameter('geometry', $point->toWKT(), 'string')
                    ->setMaxResults(1);

                return $qb->getQuery()->getSingleResult();
            }
        }
    }

    /**
     * @param Polygon $polygon
     * @param bool $exact
     * @return RuianDistrict
     * @throws NoResultException
     */
    public function findDistrictForPolygon(Polygon $polygon, bool $exact = true): RuianDistrict
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->em->getRepository(RuianDistrict::class)->createQueryBuilder('d');
        $qb
            ->setParameter('geometry', $polygon->toWKT(), 'string')
            ->setMaxResults(1);

        $or = $qb->expr()->orX();
        $or->add('ST_Within(ST_GeomFromText(:geometry), d.coords) = 1');

        if(!$exact) {
            $or->add('ST_Overlaps(ST_GeomFromText(:geometry), d.coords) = 1');
        }

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param Point $point
     * @return CompetentCity
     * @throws NoResultException
     */
    public function findCompetentCityForPoint(Point $point): CompetentCity
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->em->getRepository(CompetentCity::class)->createQueryBuilder('c');
        $qb
            ->where('ST_Within(ST_GeomFromText(:geometry), c.coords) = 1')
            ->setParameter('geometry', $point->toWKT(), 'string')
            ->setMaxResults(1);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param Polygon $polygon
     * @param bool $exact
     * @return CompetentCity
     * @throws NoResultException
     */
    public function findCompetentCityForPolygon(Polygon $polygon, bool $exact = true): CompetentCity
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->em->getRepository(CompetentCity::class)->createQueryBuilder('c');
        $qb
            ->setParameter('geometry', $polygon->toWKT(), 'string')
            ->setMaxResults(1);

        $or = $qb->expr()->orX();
        $or->add('ST_Within(ST_GeomFromText(:geometry), c.coords) = 1');

        if(!$exact) {
            $or->add('ST_Overlaps(ST_GeomFromText(:geometry), c.coords) = 1');
        }

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param Point $point
     * @return RuianCounty
     * @throws NoResultException
     */
    public function findCountyForPoint(Point $point): RuianCounty
    {
        return $this->findDistrictForPoint($point)->getCounty();
    }

    /**
     * @param Polygon $polygon
     * @return RuianCounty
     * @throws NoResultException
     */
    public function findCountyForPolygon(Polygon $polygon): RuianCounty
    {
        return $this->findDistrictForPolygon($polygon)->getCounty();
    }

    /**
     * @param Point $point
     * @return Ruian
     * @throws NoResultException
     */
    public function findAdmForPoint(Point $point): Ruian
    {
        /**
         * @var QueryBuilder $qb
         */
        $qb = $this->em->getRepository(Ruian::class)->createQueryBuilder('r');
        $qb
            ->where('ST_Equals(ST_GeomFromText(:geometry), r.coords) = 1')
            ->setParameter('geometry', $point->toWKT(), 'string')
            ->setMaxResults(1);

        return $qb->getQuery()->getSingleResult();
    }
}