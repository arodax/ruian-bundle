<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Repository;

use Arodax\RuianBundle\Entity\DataCity;
use Arodax\RuianBundle\Entity\RuianCity;
use Arodax\RuianBundle\Entity\RuianCounty;
use Arodax\RuianBundle\Entity\RuianDistrict;
use Arodax\RuianBundle\Exception\RuianException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class RuianDataProvider implements RuianDataProviderInterface
{
    protected CacheInterface $cacheRuianCoords;
    protected CacheInterface $cacheRuianData;
    protected EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em,
        CacheInterface $cacheRuianCoords,
        CacheInterface $cacheRuianData
    ) {
        $this->cacheRuianCoords = $cacheRuianCoords;
        $this->cacheRuianData = $cacheRuianData;
        $this->em = $em;
    }

    public function getCountyList(): array
    {
        return $this->cacheRuianData->get('general.county_list', function (ItemInterface $item) {
            $counties = [];

            foreach ($this->getCountyDistrictMap() as $name => $county) {
                $counties[$name] = $county[0];
            }

            return $counties;
        });
    }

    public function getDistrictList(): array
    {
        $value = $this->cacheRuianData->get('general.district_list', function (ItemInterface $item) {
            $districts = [];

            foreach ($this->getCountyDistrictMap() as $county) {
                foreach ($county[1] as $name => $district) {
                    $districts[$name] = $district;
                }
            }

            return $districts;
        });

        return $value;
    }

    public function getCountyDistrictMap(): array
    {
        return $this->cacheRuianData->get('general.mapping', function (ItemInterface $item) {
            return static::countyDistrictMap();
        });
    }

    public function getDistrictCoords(int $district): array
    {
        /**
         * @var $district RuianDistrict
         */
        $district = $this->em->getRepository(RuianDistrict::class)->find($district);

        if (empty($district)) {
            throw new RuianException("district not found");
        }

        return [
            'district_code' => $district->getCode(),
            'name' => $district->getName(),
            'coords' => $district->getCoords()->toArray(),
            'county_code' => $district->getCounty()->getCode(),
        ];
    }

    public function getCityCoords(int $city): array
    {
        /**
         * @var $city RuianCity
         */
        $city = $this->em->getRepository(RuianCity::class)->find($city);

        if (empty($city)) {
            throw new RuianException("city not found");
        }

        return [
            'city_code' => $city->getCode(),
            'name' => $city->getName(),
            'coords' => $city->getCoords()->toArray(),
            'county_code' => $city->getDistrict()->getCounty()->getCode(),
            'district_code' => $city->getDistrict()->getCode(),
        ];
    }

    public function getCityData(int $city): array
    {
        return $this->cacheRuianData->get('city.'.$city, function (ItemInterface $item) use ($city) {
            /**
             * @var $city DataCity
             */
            $city = $this->em->getRepository(DataCity::class)->findOneBy(['city' => $city]);

            if (empty($city)) {
                throw new RuianException("city not found");
            }

            return [
                'city_code' => $city->getCity()->getCode(),
                'name' => $city->getCity()->getName(),
                'population_total' => $city->getPopulationTotal(),
                'population_male' => $city->getPopulationMale(),
                'population_female' => $city->getPopulationFemale(),
                'average_age' => $city->getAverageAge(),
                'average_age_male' => $city->getAverageAgeMale(),
                'average_age_female' => $city->getAverageAgeFemale(),
            ];
        });
    }

    public function getCountyCollection(): array
    {
        $counties = [];

        foreach ($this->em->getRepository(RuianCounty::class)->findAll() as $c) {
            $districts = [];
            foreach ($this->em->getRepository(RuianDistrict::class)->findBy(['county' => $c->getId()]) as $d) {
                $districts[] = $this->getDistrictFeature((int)$d->getId());
            }

            $counties[] = [
                'type' => 'FeatureCollection',
                'features' => $districts,
                'id' => $c->getCode(),
                'properties' => [
                    'name' => $c->getName(),
                    'county_code' => $c->getCode(),
                ],
            ];
        }

        return [
            'type' => 'FeatureCollection',
            'features' => $counties,
        ];
    }

    public function getDistrictCollection(): array
    {
        $districts = [];

        //TODO: select only district_code
        foreach ($this->em->getRepository(RuianDistrict::class)->findAll() as $d) {
            $districts[] = $this->getDistrictFeature((int)$d->getId());
        }

        return [
            'type' => 'FeatureCollection',
            'features' => $districts,
        ];
    }

    public function getDistrictFeature(int $district): array
    {
        return $this->cacheRuianCoords->get('district.'.$district, function (ItemInterface $item) use ($district) {
            /**
             * @var $district RuianDistrict
             */
            $district = $this->em->getRepository(RuianDistrict::class)->find($district);

            if (empty($district)) {
                throw new RuianException("district not found");
            }

            return [
                'geometry' => [
                    'type' => 'Polygon',
                    'coordinates' => [$district->getCoords()->toArray()]
                ],
                'type' => 'Feature',
                'id' => $district->getCode(),
                'properties' => [
                    'district_code' => $district->getCode(),
                    'county_code' => $district->getCounty()->getCode(),
                    'name' => $district->getName(),
                ],
            ];
        });
    }

    public function getCityCollection(): array
    {
        $features = [];

        //TODO: select only district_code
        foreach ($this->em->getRepository(RuianCity::class)->findAll() as $c) {
            $features[] = $this->getCityFeature((int)$c->getId());
        }

        return [
            'type' => 'FeatureCollection',
            'features' => $features,
        ];
    }

    public function getCityFeature(int $city): array
    {
        return $this->cacheRuianCoords->get('city.'.$city, function (ItemInterface $item) use ($city) {
            /**
             * @var $city RuianCity
             */
            $city = $this->em->getRepository(RuianCity::class)->find($city);

            if (empty($city)) {
                throw new RuianException("city not found");
            }

            return [
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => $city->getCoords()->toArray()
                ],
                'type' => 'Feature',
                'id' => $city->getCode(),
                'properties' => array_merge($this->getCityData($city->getCode()), [
                    'county_code' => $city->getDistrict()->getCounty()->getCode(),
                    'district_code' => $city->getDistrict()->getCode(),
                    'city_code' => $city->getCode(),
                    'name' => $city->getName(),
                ]) ,
            ];
        });
    }

    public static function countyDistrictMap(): array
    {
        return [
            'hlavni_mesto_praha' => [
                3018,
                ['praha' => 40924]
            ],
            'jihocesky_kraj' => [
                3034,
                [
                    'ceske_budejovice' => 40282,
                    'cesky_krumlov' => 40291,
                    'jindrichuv_hradec' => 40304,
                    'pisek' => 40312,
                    'prachatice' => 40321,
                    'strakonice' => 40339,
                    'tabor' => 40347,
                ]
            ],
            'jihomoravsky_kraj' => [
                3115,
                [
                    'blansko' => 40703,
                    'breclav' => 40738,
                    'brno_mesto' => 40711,
                    'brno_venkov' => 40720,
                    'hodonin' => 40746,
                    'vyskov' => 40754,
                    'znojmo' => 40762,
                ]
            ],
            'karlovarsky_kraj' => [
                3051,
                [
                    'cheb' => 40428,
                    'karlovy_vary' => 40436,
                    'sokolov' => 40444,
                ]
            ],
            'kraj_vysocina' => [
                3017,
                [
                    'havlickuv_brod' => 40657,
                    'jihlava' => 40665,
                    'pelhrimov' => 40673,
                    'trebic' => 40681,
                    'zdar_nad_sazavou' => 40690,
                ]
            ],
            'kralovohradecky_kraj' => [
                3085,
                [
                    'hradec_kralove' => 40568,
                    'jicin' => 40576,
                    'nachod' => 40584,
                    'rychnov_nad_kneznou' => 40592,
                    'trutnov' => 40606,
                ]
            ],
            'liberecky_kraj' => [
                3077,
                [
                    'ceska_lipa' => 40525,
                    'jablonec_nad_nisou' => 40533,
                    'liberec' => 40541,
                    'semily' => 40550,
                ]
            ],
            'moravskoslezsky_kraj' => [
                3140,
                [
                    'bruntal' => 40860,
                    'frydek_mistek' => 40878,
                    'karvina' => 40886,
                    'novy_jicin' => 40894,
                    'opava' => 40908,
                    'ostrava_mesto' => 40916,
                ]
            ],
            'olomoucky_kraj' => [
                3123,
                [
                    'jesenik' => 40771,
                    'olomouc' => 40789,
                    'prerov' => 40801,
                    'prostejov' => 40797,
                    'sumperk' => 40819,
                ]
            ],
            'pardubicky_kraj' => [
                3093,
                [
                    'chrudim' => 40614,
                    'pardubice' => 40622,
                    'svitavy' => 40631,
                    'usti_nad_orlici' => 40649,
                ]
            ],
            'plzensky_kraj' => [
                3042,
                [
                    'domazlice' => 40355,
                    'klatovy' => 40363,
                    'plzen_jih' => 40380,
                    'plzen_mesto' => 40371,
                    'plzen_sever' => 40398,
                    'rokycany' => 40401,
                    'tachov' => 40410,
                ]
            ],
            'stredocesky_kraj' => [
                3026,
                [
                    'benesov' => 40169,
                    'beroun' => 40177,
                    'kladno' => 40185,
                    'kolin' => 40193,
                    'kutna_hora' => 40207,
                    'melnik' => 40215,
                    'mlada_boleslav' => 40223,
                    'nymburk' => 40231,
                    'praha_vychod' => 40240,
                    'praha_zapad' => 40258,
                    'pribram' => 40266,
                    'rakovnik' => 40274,
                ]
            ],
            'ustecky_kraj' => [
                3069,
                [
                    'chomutov' => 40461,
                    'decin' => 40452,
                    'litomerice' => 40479,
                    'louny' => 40487,
                    'most' => 40495,
                    'teplice' => 40509,
                    'usti_nad_labem' => 40517,
                ]
            ],
            'zlinsky_kraj' => [
                3131,
                [
                    'kromeriz' => 40827,
                    'uherske_hradiste' => 40835,
                    'vsetin' => 40843,
                    'zlin' => 40851,
                ]
            ]
        ];
    }
}
