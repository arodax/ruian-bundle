<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Repository;

use Arodax\RuianBundle\Entity\Ruian;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;

/**
 * @method Ruian|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ruian|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ruian[]    findAll()
 * @method Ruian[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RuianRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ruian::class);
    }

    /**
     * @param int $adm
     *
     * @return Ruian|null
     */
    public function findOneByAdm(int $adm): ?Ruian
    {
        return $this->findOneBy(['adm' => $adm]);
    }

    /**
     * Return one random Ruian.
     *
     * @throws NoResultException
     *
     * @return Ruian
     */
    public function findOneRandom(): Ruian
    {
        $cnt = $this->createQueryBuilder('r')
            ->select('COUNT(r.adm) AS cnt')
            ->getQuery()->getSingleScalarResult();

        if(!($cnt > 0)) {
            throw new NoResultException();
        }

        return $this->createQueryBuilder('r')
            ->setMaxResults(1)
            ->setFirstResult(rand(0, $cnt-1))
            ->getQuery()->getSingleResult();
    }

    public function getCountyNames(): array
    {
        return ['Benešov', 'Beroun', 'Blansko', 'Břeclav', 'Brno-město', 'Brno-venkov', 'Bruntál', 'Česká Lípa', 'České Budějovice', 'Český Krumlov', 'Cheb', 'Chomutov', 'Chrudim', 'Děčín', 'Domažlice', 'Frýdek-Místek', 'Havlíčkův Brod', 'Hodonín', 'Hradec Králové', 'Jablonec nad Nisou', 'Jeseník', 'Jičín', 'Jihlava', 'Jindřichův Hradec', 'Karlovy Vary', 'Karviná', 'Kladno', 'Klatovy', 'Kolín', 'Kroměříž', 'Kutná Hora', 'Liberec', 'Litoměřice', 'Louny', 'Mělník', 'Mladá Boleslav', 'Most', 'Náchod', 'Nový Jičín', 'Nymburk', 'Olomouc', 'Opava', 'Ostrava-město', 'Pardubice', 'Pelhřimov', 'Písek', 'Plzeň-jih', 'Plzeň-město', 'Plzeň-sever', 'Prachatice', 'Praha', 'Praha-východ', 'Praha-západ', 'Přerov', 'Příbram', 'Prostějov', 'Rakovník', 'Rokycany', 'Rychnov nad Kněžnou', 'Semily', 'Sokolov', 'Strakonice', 'Šumperk', 'Svitavy', 'Tábor', 'Tachov', 'Teplice', 'Třebíč', 'Trutnov', 'Uherské Hradiště', 'Ústí nad Labem', 'Ústí nad Orlicí', 'Vsetín', 'Vyškov', 'Žďár nad Sázavou', 'Zlín', 'Znojmo'];
    }

    public function searchAdm(int $search)
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->where($qb->expr()->eq('r.adm', (int) $search))
            ->setMaxResults(20)
//            ->where("MATCH(city_name, street_name, district_name) AGAINST ('%{$search}%' IN NATURAL LANGUAGE MODE)")
        ;

        return $qb->getQuery()->getResult();
    }

    public function searchCity(string $search)
    {
        $qb = $this->createQueryBuilder('r');
        $qb
            ->select('MIN(r.cityName) AS city_name')
            ->where($qb->expr()->like('r.cityName', $qb->expr()->literal('%'.$search.'%')))
            ->groupBy('r.cityName')
            ->setMaxResults(20)
//            ->where("MATCH(city_name, street_name, district_name) AGAINST ('%{$search}%' IN NATURAL LANGUAGE MODE)")
        ;

        return $qb->getQuery()->getResult();
    }

    public function searchCounty(string $search)
    {
        $found = preg_grep(sprintf('/%s/i', $search), $this->getCounties());
        $found = array_values(array_map(function ($value) {
            return ['county_name' => $value];
        }, $found));
        return $found;
    }

    public function getRegionNames(): array
    {
        return [
            'Hlavní město Praha' => [
                'Praha'
            ],
            'Jihočeský kraj' => [
                'České Budějovice',
                'Český Krumlov',
                'Jindřichův Hradec',
                'Písek',
                'Prachatice',
                'Strakonice',
                'Tábor'
            ],
            'Jihomoravský kraj' => [
                'Blansko',
                'Břeclav',
                'Brno-město',
                'Brno-venkov',
                'Hodonín',
                'Vyškov',
                'Znojmo',
            ],
            'Karlovarský kraj' => [
                'Cheb',
                'Karlovy Vary',
                'Sokolov',
            ],
            'Kraj Vysočina' => [
                'Havlíčkův Brod',
                'Jihlava',
                'Pelhřimov',
                'Třebíč',
                'Žďár nad Sázavou',
            ],
            'Královéhradecký kraj' => [
                'Hradec Králové',
                'Jičín',
                'Náchod',
                'Rychnov nad Kněžnou',
                'Trutnov',
            ],
            'Liberecký kraj' => [
                'Česká Lípa',
                'Jablonec nad Nisou',
                'Liberec',
                'Semily',
            ],
            'Moravskoslezský kraj' => [
                'Bruntál',
                'Frýdek-Místek',
                'Karviná',
                'Nový Jičín',
                'Opava',
                'Ostrava-město',
            ],
            'Olomoucký kraj' => [
                'Jeseník',
                'Olomouc',
                'Přerov',
                'Prostějov',
                'Šumperk',
            ],
            'Pardubický kraj' => [
                'Chrudim',
                'Pardubice',
                'Svitavy',
                'Ústí nad Orlicí',
            ],
            'Plzeňský kraj' => [
                'Domažlice',
                'Klatovy',
                'Plzeň-jih',
                'Plzeň-město',
                'Plzeň-sever',
                'Rokycany',
                'Tachov',
            ],
            'Středočeský kraj' => [
                'Benešov',
                'Beroun',
                'Kladno',
                'Kolín',
                'Kutná Hora',
                'Mělník',
                'Mladá Boleslav',
                'Nymburk',
                'Praha-východ',
                'Praha-západ',
                'Příbram',
                'Rakovník',
            ],
            'Ústecký kraj' => [
                'Chomutov',
                'Děčín',
                'Litoměřice',
                'Louny',
                'Most',
                'Teplice',
                'Ústí nad Labem',
            ],
            'Zlínský kraj' => [
                'Kroměříž',
                'Uherské Hradiště',
                'Vsetín',
                'Zlín',
            ]
        ];
    }
}
