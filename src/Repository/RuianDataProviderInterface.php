<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Repository;

interface RuianDataProviderInterface
{
    public function getCountyList(): array;
    public function getDistrictList(): array;
    public function getCountyDistrictMap(): array;
    public function getDistrictCoords(int $district): array;
    public function getCityCoords(int $city): array;
    public function getCityData(int $city): array;
    public static function countyDistrictMap(): array;

    public function getCountyCollection(): array;
    public function getDistrictCollection(): array;
    public function getDistrictFeature(int $district): array;
    public function getCityCollection(): array;
    public function getCityFeature(int $city): array;
}
