<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle;

use Arodax\RuianBundle\DependencyInjection\Compiler\FosElasticaExtensionPass;
use Arodax\RuianBundle\DependencyInjection\Compiler\SymfonyExtensionPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ArodaxRuianBundle extends Bundle
{
    public function build(ContainerBuilder $builder)
    {
        $builder->addCompilerPass(new FosElasticaExtensionPass());
        $builder->addCompilerPass(new SymfonyExtensionPass());
    }
}
