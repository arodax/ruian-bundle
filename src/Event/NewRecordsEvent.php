<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class NewRecordsEvent extends Event
{
    private array $records;

    public function __construct(array $records)
    {
        $this->records = $records;
    }

    public function getRecords(): array
    {
        return $this->records;
    }
}
