<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Arodax\Doctrine\Spatial\ValueObject\Polygon;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_map_district')]
class RuianDistrict
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(type: "integer")]
    protected ?int $districtCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $districtName;

    #[ORM\ManyToOne(targetEntity: RuianCounty::class)]
    #[ORM\JoinColumn(referencedColumnName: "county_code", onDelete: "RESTRICT")]
    protected ?RuianCounty $county;

    #[ORM\ManyToOne(targetEntity: RuianCity::class)]
    #[ORM\JoinColumn(referencedColumnName: "city_code", onDelete: "RESTRICT")]
    protected ?RuianCity $city;

    #[ORM\Column(type: "polygon", nullable: true)]
    protected ?Polygon $coords;

    #[ORM\Column(type: "string")]
    protected ?string $slug;

    public function getId()
    {
        return $this->getCode();
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getCode')]
    public function getDistrictCode(): ?int
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getDistrictCode', 'getCode');

        return $this->districtCode;
    }

    public function getCode(): ?int
    {
        return $this->districtCode;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setCode')]
    public function setDistrictCode(int $code): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setDistrictCode', 'setCode');

        $this->districtCode = $code;

        return $this;
    }

    public function setCode(int $code): self
    {
        $this->districtCode = $code;

        return $this;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getName')]
    public function getDistrictName(): ?string
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getDistrictName', 'getName');

        return $this->districtName;
    }

    public function getName(): ?string
    {
        return $this->districtName;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setName')]
    public function setDistrictName(string $name): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setDistrictName', 'setName');

        $this->districtName = $name;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->districtName = $name;

        return $this;
    }

    public function getCoords(): ?Polygon
    {
        return $this->coords;
    }

    /**
     * @param array $coords [float $latitude, float $longitude]
     */
    public function setCoords(array $coords): self
    {
        $this->coords = new Polygon($coords);
        return $this;
    }

    public function getCounty(): ?RuianCounty
    {
        return $this->county;
    }

    public function setCounty(?RuianCounty $county): self
    {
        $this->county = $county;
        return $this;
    }

    public function getCity(): ?RuianCity
    {
        return $this->city;
    }

    public function setCity(?RuianCity $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }
}
