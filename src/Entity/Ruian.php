<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Arodax\Doctrine\Spatial\ValueObject\Point;
use Arodax\RuianBundle\Repository\RuianRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass:  RuianRepository::class)]
#[ORM\Table(name: 'ruian_map_adm')]
class Ruian
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(type: "integer")]
    protected int $adm;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $cityCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $cityName;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $momcCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $momcName;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $mopCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $mopName;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $districtCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $districtName;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $streetCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $streetName;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $so;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $houseNumber;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $streetNumber;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $streetNumberCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $postcode;

    #[ORM\Column(type: "integer", nullable: false)]
    protected ?int $countyCode;

    #[ORM\Column(type: "string", nullable: false)]
    protected ?string $countyName;

    #[ORM\Column(type: "decimal", nullable: true)]
    protected ?float $x;

    #[ORM\Column(type: "decimal", nullable: true)]
    protected ?float $y;

    #[ORM\Column(type: "datetime", nullable: true)]
    protected ?\DateTimeInterface $dateOfCreation;

    #[ORM\Column(type: "point", nullable: true)]
    protected ?Point $coords;

    #[ORM\Column(type: "boolean", nullable: false, options: ["default" => 1])]
    protected ?bool $enabled;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getAdm();
    }

    /**
     * @return int
     */
    public function getAdm(): int
    {
        return $this->adm;
    }

    /**
     * @param int $adm
     *
     * @return Ruian
     */
    public function setAdm(int $adm): self
    {
        $this->adm = $adm;

        return $this;
    }

    /**
     * @return int
     */
    public function getCityCode(): ?int
    {
        return $this->cityCode;
    }

    /**
     * @param int|null $cityCode
     *
     * @return Ruian
     */
    public function setCityCode(?int $cityCode): self
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    /**
     * @param string|null $cityName
     *
     * @return Ruian
     */
    public function setCityName(?string $cityName): self
    {
        $this->cityName = $cityName;

        return $this;
    }

    /**
     * @return int
     */
    public function getMomcCode(): ?int
    {
        return $this->momcCode;
    }

    /**
     * @param int $momcCode
     *
     * @return Ruian
     */
    public function setMomcCode(?int $momcCode): self
    {
        $this->momcCode = $momcCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getMomcName(): ?string
    {
        return $this->momcName;
    }

    /**
     * @param string $momcName
     *
     * @return Ruian
     */
    public function setMomcName(?string $momcName): self
    {
        $this->momcName = $momcName;

        return $this;
    }

    /**
     * @return int
     */
    public function getMopCode(): ?int
    {
        return $this->mopCode;
    }

    /**
     * @param int $mopCode
     *
     * @return Ruian
     */
    public function setMopCode(?int $mopCode): self
    {
        $this->mopCode = $mopCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getMopName(): ?string
    {
        return $this->mopName;
    }

    /**
     * @param string $mopName
     *
     * @return Ruian
     */
    public function setMopName(?string $mopName): self
    {
        $this->mopName = $mopName;

        return $this;
    }

    /**
     * @return int
     */
    public function getDistrictCode(): ?int
    {
        return $this->districtCode;
    }

    /**
     * @param int $districtCode
     *
     * @return Ruian
     */
    public function setDistrictCode(?int $districtCode): self
    {
        $this->districtCode = $districtCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getDistrictName(): ?string
    {
        return $this->districtName;
    }

    /**
     * @param string $districtName
     *
     * @return Ruian
     */
    public function setDistrictName(?string $districtName): self
    {
        $this->districtName = $districtName;

        return $this;
    }

    /**
     * @return int
     */
    public function getStreetCode(): ?int
    {
        return $this->streetCode;
    }

    /**
     * @param int $streetCode
     *
     * @return Ruian
     */
    public function setStreetCode(?int $streetCode): self
    {
        $this->streetCode = $streetCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    /**
     * @param string $streetName
     *
     * @return Ruian
     */
    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSo(): ?string
    {
        return $this->so;
    }

    /**
     * @param string $so
     *
     * @return Ruian
     */
    public function setSo(?string $so): self
    {
        $this->so = $so;

        return $this;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     *
     * @return Ruian
     */
    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetNumber(): ?string
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     *
     * @return Ruian
     */
    public function setStreetNumber(?string $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getStreetNumberCode(): ?int
    {
        return $this->streetNumberCode;
    }

    /**
     * @param int $streetNumberCode
     *
     * @return Ruian
     */
    public function setStreetNumberCode(?int $streetNumberCode): self
    {
        $this->streetNumberCode = $streetNumberCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @param string|null $postcode
     *
     * @return Ruian
     */
    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountyCode(): int
    {
        return $this->countyCode;
    }

    /**
     * @param int $countyCode
     *
     * @return Ruian
     */
    public function setCountyCode(int $countyCode): self
    {
        $this->countyCode = $countyCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountyName(): string
    {
        return $this->countyName;
    }

    /**
     * @param string $countyName
     *
     * @return Ruian
     */
    public function setCountyName(string $countyName): self
    {
        $this->countyName = $countyName;

        return $this;
    }

    /**
     * @return float
     */
    public function getX(): ?float
    {
        return $this->x;
    }

    /**
     * @param float|null $x
     *
     * @return Ruian
     */
    public function setX(?float $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @return float
     */
    public function getY(): ?float
    {
        return $this->y;
    }

    /**
     * @param float|null $y
     *
     * @return Ruian
     */
    public function setY(?float $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateOfCreation(): \DateTimeInterface
    {
        return $this->dateOfCreation;
    }

    /**
     * @param \DateTimeInterface $dateOfCreation
     *
     * @return Ruian
     */
    public function setDateOfCreation(\DateTimeInterface $dateOfCreation): self
    {
        $this->dateOfCreation = $dateOfCreation;

        return $this;
    }

    /**
     * @return Point|null
     */
    public function getCoords(): ?Point
    {
        return $this->coords;
    }

    /**
     * @param array $coords [float longitude, float latitude ]
     * @return self
     */
    public function setCoords(array $coords): self
    {
        $this->coords = new Point((float)$coords[0], (float)$coords[1]);
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool)$this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Ruian
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }
}
