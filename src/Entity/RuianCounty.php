<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_map_county')]
class RuianCounty
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(type: "integer")]
    protected ?int $countyCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $countyName;

    #[ORM\Column(type: "string")]
    protected ?string $slug;

    public function getId(): ?int
    {
        return $this->getCode();
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getCode')]
    public function getCountyCode(): ?int
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getCountyCode', 'getCode');

        return $this->countyCode;
    }

    public function getCode(): ?int
    {
        return $this->countyCode;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setCode')]
    public function setCountyCode(int $code): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setCountyCode', 'setCode');

        $this->countyCode = $code;

        return $this;
    }

    public function setCode(int $code): self
    {
        $this->countyCode = $code;

        return $this;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getName')]
    public function getCountyName(): ?string
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getCountyName', 'getName');

        return $this->countyName;
    }

    public function getName(): ?string
    {
        return $this->countyName;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setName')]
    public function setCountyName(string $name): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setCountyName', 'setName');

        $this->countyName = $name;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->countyName = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }
}
