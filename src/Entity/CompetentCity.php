<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Arodax\Doctrine\Spatial\ValueObject\MultiPolygon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_competent_city')]
class CompetentCity
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(
        name: "orp_code",
        type: "integer",
        options: ["unsigned" => true]
    )]
    protected ?int $orpCode;

    #[ORM\OneToOne(inversedBy: "competent", targetEntity: RuianCity::class)]
    #[ORM\JoinColumn(referencedColumnName: "city_code", onDelete: "RESTRICT")]
    protected ?RuianCity $city;

    #[ORM\ManyToMany(targetEntity: RuianCity::class, cascade: ["remove", "persist"])]
    #[ORM\JoinTable(
        name: "ruian_competent_city_member",
        joinColumns: [
            new ORM\JoinColumn(
                name: "orp_code",
                referencedColumnName: "orp_code",
                onDelete: "CASCADE"
            )
        ],
        inverseJoinColumns: [
            new ORM\JoinColumn(
                name: "member_city_code",
                referencedColumnName: "city_code",
                unique: true,
                onDelete: "CASCADE"
            )
        ]
    )]
    protected $memberCities;

    #[ORM\Column(type: "multipolygon", nullable: true)]
    protected ?MultiPolygon $coords;

    public function __construct()
    {
        $this->memberCities = new ArrayCollection();
    }

    public function getId()
    {
        return $this->getOrpCode();
    }

    public function getOrpCode(): ?int
    {
        return $this->orpCode;
    }

    public function setOrpCode(int $orpCode): self
    {
        $this->orpCode = $orpCode;
        return $this;
    }

    public function setCity(RuianCity $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getCity(): ?RuianCity
    {
        return $this->city;
    }

    public function addMemberCity(RuianCity $city): self
    {
        if (!$this->memberCities->contains($city)) {
            $this->memberCities->add($city);
        }

        return $this;
    }

    public function getMemberCities(): array
    {
        return $this->memberCities->toArray();
    }

    public function getCoords(): ?MultiPolygon
    {
        return $this->coords;
    }

    public function setCoords(array $coords): self
    {
        $this->coords = new MultiPolygon($coords);
        return $this;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }
}
