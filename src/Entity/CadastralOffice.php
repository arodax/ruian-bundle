<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_cadastral_office')]
class CadastralOffice
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(
        name: "code",
        type: "integer",
        options: ["unsigned" => true]
    )]
    protected ?int $code;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $cityName;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $regionName;

    #[ORM\Column(type: "string", nullable: false)]
    protected ?string $countyName;

    #[ORM\Column(type: "string", nullable: false)]
    protected ?string $officeName;
    
    public function getId()
    {
        return $this->getCode();
    }

    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    public function setCityName(string $cityName): self
    {
        $this->cityName = $cityName;
        return $this;
    }

    public function getRegionName(): ?string
    {
        return $this->regionName;
    }

    public function setRegionName(string $regionName): self
    {
        $this->regionName = $regionName;
        return $this;
    }

    public function getCountyName(): ?string
    {
        return $this->countyName;
    }

    public function setCountyName(string $countyName): self
    {
        $this->countyName = $countyName;
        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $officeCode): self
    {
        $this->code = $officeCode;
        return $this;
    }

    public function getOfficeName(): ?string
    {
        return $this->officeName;
    }

    public function setOfficeName(string $officeName): self
    {
        $this->officeName = $officeName;
        return $this;
    }
}
