<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Arodax\Doctrine\Spatial\ValueObject\Point;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Deprecated;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_map_city')]
class RuianCity
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "NONE")]
    #[ORM\Column(type: "integer")]
    protected ?int $cityCode;

    #[ORM\Column(type: "string", nullable: true)]
    protected ?string $cityName;

    #[ORM\ManyToOne(targetEntity: RuianDistrict::class)]
    #[ORM\JoinColumn(referencedColumnName: "district_code", onDelete: "RESTRICT")]
    protected ?RuianDistrict $district;

    #[ORM\Column(type: "point", nullable: true)]
    protected ?Point $coords;

    #[ORM\OneToOne(mappedBy: "city", targetEntity: CompetentCity::class)]
    protected ?CompetentCity $competent;

    #[ORM\OneToOne(mappedBy: "city", targetEntity: DataCity::class)]
    protected DataCity $data;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getCode();
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getCode')]
    public function getCityCode(): ?int
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getCityCode', 'getCode');

        return $this->cityCode;
    }

    public function getCode(): ?int
    {
        return $this->cityCode;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setCode')]
    public function setCityCode(int $code): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setCityCode', 'setCode');

        $this->cityCode = $code;

        return $this;
    }

    public function setCode(int $code): self
    {
        $this->cityCode = $code;

        return $this;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'getName')]
    public function getCityName(): ?string
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'getCityName', 'getName');

        return $this->cityName;
    }

    public function getName(): ?string
    {
        return $this->cityName;
    }

    #[Deprecated(reason: 'Using "%s" is deprecated, use "%s" instead.', replacement: 'setName')]
    public function setCityName(string $name): self
    {
        trigger_deprecation('arodax/admin-bundle', '3.0.3', 'Using "%s" is deprecated, use "%s" instead.', 'setCityName', 'setName');

        $this->cityName = $name;

        return $this;
    }

    public function setName(string $name): self
    {
        $this->cityName = $name;

        return $this;
    }

    public function getDistrict(): ?RuianDistrict
    {
        return $this->district;
    }

    public function setDistrict(?RuianDistrict $district): void
    {
        $this->district = $district;
    }

    public function getCoords(): ?Point
    {
        return $this->coords;
    }

    /**
     * @param array $coords [float longitude, float latitude]
     */
    public function setCoords(array $coords): self
    {
        $this->coords = new Point((float)$coords[0], (float)$coords[1]);
        return $this;
    }

    public function getCompetent(): ?CompetentCity
    {
        return $this->competent;
    }

    public function __toString(): string
    {
        return (string)$this->getId();
    }

    public function getData(): DataCity
    {
        return $this->data;
    }

    public function setData(DataCity $data): self
    {
        $this->data = $data;
        
        return $this;
    }
}
