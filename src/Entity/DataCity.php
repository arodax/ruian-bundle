<?php

/*
 * This file is part of the ruian-bundle package.
 *
 * (c) ARODAX a.s.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Arodax\RuianBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'ruian_data_city')]
class DataCity
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    #[ORM\Column(type: "integer", options: ["unsigned" => true])]
    protected int $id;

    #[ORM\OneToOne(inversedBy: "data", targetEntity: RuianCity::class)]
    #[ORM\JoinColumn(referencedColumnName: "city_code", onDelete: "RESTRICT")]
    protected ?RuianCity $city;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $populationTotal;


    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $populationMale;

    #[ORM\Column(type: "integer", nullable: true)]
    protected ?int $populationFemale;

    #[ORM\Column(type: "decimal", precision: 3, scale: 1, nullable: true)]
    protected ?float $averageAge;

    #[ORM\Column(type: "decimal", precision: 3, scale: 1, nullable: true)]
    protected ?float $averageAgeMale;

    #[ORM\Column(type: "decimal", precision: 3, scale: 1, nullable: true)]
    protected ?float $averageAgeFemale;

    public function getId()
    {
        return $this->id;
    }

    public function setCity(RuianCity $city): self
    {
        $this->city = $city;
        return $this;
    }

    public function getCity(): ?RuianCity
    {
        return $this->city;
    }

    public function getPopulationTotal(): ?int
    {
        return $this->populationTotal;
    }

    public function setPopulationTotal(?int $populationTotal): self
    {
        $this->populationTotal = $populationTotal;
        return $this;
    }

    public function getPopulationMale(): ?int
    {
        return $this->populationMale;
    }

    public function setPopulationMale(?int $populationMale): self
    {
        $this->populationMale = $populationMale;
        return $this;
    }

    public function getPopulationFemale(): ?int
    {
        return $this->populationFemale;
    }

    public function setPopulationFemale(?int $populationFemale): self
    {
        $this->populationFemale = $populationFemale;
        return $this;
    }

    public function getAverageAge(): ?float
    {
        return $this->averageAge;
    }

    public function setAverageAge(?float $averageAge): self
    {
        $this->averageAge = $averageAge;
        return $this;
    }

    public function getAverageAgeMale(): ?float
    {
        return $this->averageAgeMale;
    }

    public function setAverageAgeMale(?float $averageAgeMale): self
    {
        $this->averageAgeMale = $averageAgeMale;
        return $this;
    }

    public function getAverageAgeFemale(): ?float
    {
        return $this->averageAgeFemale;
    }

    public function setAverageAgeFemale(?float $averageAgeFemale): self
    {
        $this->averageAgeFemale = $averageAgeFemale;
        return $this;
    }
}
