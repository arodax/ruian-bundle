<?php

declare(strict_types=1);

namespace Arodax\RuianBundle\DependencyInjection\Compiler;

use Arodax\RuianBundle\EventListener\ElasticaListener;
use Arodax\RuianBundle\Search\RuianAdmSearchElasticaProvider;
use Arodax\RuianBundle\Search\RuianAdmSearchProviderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class FosElasticaExtensionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $index = 'ruian';

        if ($container->hasParameter('ruian_bundle.index_name')) {
            $index = $container->getParameter('ruian_bundle.index_name');
        }

        try {
            $persister = $container->findDefinition('fos_elastica.object_persister.'.$index);
            $elasticaListener = new Definition();
            $elasticaListener
                ->setAutoconfigured(true)
                ->setAutowired(true)
                ->setClass(ElasticaListener::class)
                ->addTag('kernel.event_subscriber')
                ->setArgument('$persister', $persister);

            $container->setDefinition(ElasticaListener::class, $elasticaListener);
        } catch (ServiceNotFoundException $e) {
        }

        try {
            $finder = $container->findDefinition('fos_elastica.finder.'.$index);
            $searchProvider = new Definition();
            $searchProvider
                ->setAutoconfigured(true)
                ->setAutowired(true)
                ->setClass(RuianAdmSearchElasticaProvider::class)
                ->setArgument('$finder', $finder);

            $container->setDefinition(RuianAdmSearchElasticaProvider::class, $searchProvider);
            $container->setAlias(RuianAdmSearchProviderInterface::class, new Alias(RuianAdmSearchElasticaProvider::class, false));
        } catch (ServiceNotFoundException $e) {
        }
    }
}
