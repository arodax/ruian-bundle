<?php

declare(strict_types=1);

namespace Arodax\RuianBundle\DependencyInjection\Compiler;

use Arodax\RuianBundle\Command\InstallCommand;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class SymfonyExtensionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        try {
            $slugger = $container->findDefinition('Symfony\Component\String\Slugger\SluggerInterface');
            $installCommand = new Definition();
            $installCommand
                ->setAutoconfigured(true)
                ->setAutowired(true)
                ->setClass(InstallCommand::class)
                ->addTag('console.command')
                ->setArgument('$slugger', $slugger);

            $container->setDefinition(InstallCommand::class, $installCommand);
        } catch (ServiceNotFoundException $e) {
        }
    }
}
