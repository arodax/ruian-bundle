# Symfony RÚIAN bundle (arodax/ruian-bundle)

Supported Versions
------------------

------------
| Version         | Supported | 
| ----------------| ----------|
| 3.x             | Yes
| 2.x             | No 
| 1.x             | No
------------

Changelog
------------------
### 3.2.2
- Minor serialization groups updates
### 3.2.1
- Updated documentation
### 3.2.0
- Updated RÚIAN data
### 3.1.0
- [BC] Renamed method getCounties -> getCountyNames
- [BC] Renamed method getRegions -> getRegionNames
- [Feature] Added slug to county and district entities **update your database schema!**
- [BC] Removed SQL files, they are located in arodax/ruian-data repository

### 3.0.4
- Added Symfoy LTS (4.4) support

### 3.0.3
- Deprecated get/setCountyName, get/setDistrictName, get/setCityName() use getName and setName
- Deprecated get/setCountyCode, get/setDistrictCode, get/setCityCode() use getCode and setCode

### 3.0.2
- Fixed incorrect SQL statement in `arodax_ruian:update_dbal`

### 3.0.1
- Use default Elastic Search index name if not set otherwise.

### 3.0.0
- [BC break] REMOVED`bin/console arodax_ruian:update` is  use `arodax_ruian:update_dbal` or `arodax_ruian:update_orm`
- [feature] Allow to use faster DBAL import `bin/console arodax_ruian:update_dbal` which does not trigger ORM functions
- [feature] Automatically detect `friendsofsymfony/elastica-bundle`, if available index RÚIAN data into Elastic Search
